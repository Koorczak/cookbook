insert into app_user (USER_ID, EMAIL, ENABLED, FIRST_NAME, LAST_NAME, PASSWORD) values ('100','maciej.zaporowski@lublin.eu', 1, 'Maciek', 'Zaporowski', '$2a$10$.B0c6faNohNsUFZEgWiac.R7sBN/t.tfv7TcDg4XvD/xG5lmFUIUK');
insert into app_user (USER_ID, EMAIL, ENABLED, FIRST_NAME, LAST_NAME, PASSWORD) values ('101','maciekzaporowski@gmail.com', 1, 'Maciek', 'Zaporowski', '$2a$10$.B0c6faNohNsUFZEgWiac.R7sBN/t.tfv7TcDg4XvD/xG5lmFUIUK');
INSERT INTO ROLES (ROLE_ID, ROLE) VALUE (1,'Admin');
INSERT INTO ROLES (ROLE_ID, ROLE) VALUE (2,'User');
insert into RECIPE (RECIPE_ID, RECIPE_AUTHOR, RECIPE_GROUP, RECIPE_INGREDIENT, RECIPE_TITLE) values ('110', 'Maciek', 'Dinner', 'pomidory', 'Pomidorowa');
insert into RECIPE (RECIPE_ID, RECIPE_AUTHOR, RECIPE_GROUP, RECIPE_INGREDIENT, RECIPE_TITLE) values ('111', 'Przemek', 'Dinner', 'kluski', 'Minestrone');

