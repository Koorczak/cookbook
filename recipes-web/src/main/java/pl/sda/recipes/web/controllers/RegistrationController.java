package pl.sda.recipes.web.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.sda.recipes.dao.DTO.RegistrationDTO;
import pl.sda.recipes.service.user.command.UserCommandService;
import pl.sda.recipes.service.user.query.UserQueryService;

import javax.validation.Valid;

@Controller
public class RegistrationController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RegistrationController.class);

    private final UserCommandService userCommandService;
    private final UserQueryService userQueryService;


    @Autowired
    public RegistrationController(UserCommandService userCommandService, UserQueryService userQueryService) {
        this.userCommandService = userCommandService;
        this.userQueryService = userQueryService;
    }


    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String newUser(@ModelAttribute("flashUser") RegistrationDTO registrationDTO, Model model) {
        model.addAttribute("registration", registrationDTO);
        return "registration";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String saveUser(@Valid @ModelAttribute("registration") RegistrationDTO registrationDTO, BindingResult result) {
        if (result.hasErrors()) {
            return "registration";
        }

        userCommandService.create(registrationDTO);

        LOGGER.debug("User " + registrationDTO.getFirstName() + " " + registrationDTO.getLastName() + " registered successfully");

//        model.addAttribute("success", "User " + user.getFirstName() + " " + user.getLastName() + " registered successfully");
        //return "success";
        return "redirect:/login";
    }
}
