package pl.sda.recipes.web.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
class SecurityConfig extends WebSecurityConfigurerAdapter {


    @Autowired
    private DataSource dataSource;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
//                .antMatchers("/").anonymous()
                .antMatchers("/login*").anonymous()
                .antMatchers("/addrecipe*").anonymous()
                .antMatchers("/register*").anonymous()
                .antMatchers("/resources/**").permitAll()
                .antMatchers("/**").authenticated()
                .and().formLogin()
                    .loginPage("/login")
                    .defaultSuccessUrl("/main")
                    .loginProcessingUrl("/perform_login")
                    .failureUrl("/login?error")
                    .usernameParameter("email")
                    .passwordParameter("password")
                .and().logout().
                    logoutUrl("/perform_logout").
                    logoutSuccessUrl("/login?logout");
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/resources/**");
    }

    public void configure(AuthenticationManagerBuilder authenticationManager) throws Exception {
//        authenticationManager.inMemoryAuthentication()
//                .withUser("admin@admin.pl").password("admin").roles("USER");

        authenticationManager.jdbcAuthentication()
                .dataSource(dataSource)
                .usersByUsernameQuery("select email, password, enabled from app_user where email = ?")
                .authoritiesByUsernameQuery("select email, 'ROLE_USER' from app_user where email = ?")
                .passwordEncoder(passwordEncoder);
    }

}
