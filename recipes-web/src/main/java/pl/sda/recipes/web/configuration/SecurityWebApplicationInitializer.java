package pl.sda.recipes.web.configuration;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

    public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {

        public SecurityWebApplicationInitializer() {
            super();
        }
    }

