package pl.sda.recipes.web.controllers;

import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.sda.recipes.dao.DTO.RegistrationDTO;
import pl.sda.recipes.dao.domain.Recipe;
import pl.sda.recipes.service.recipe.command.RecipeCommandService;
import pl.sda.recipes.service.recipe.query.RecipeQueryService;
import pl.sda.recipes.service.user.query.UserQueryService;

import javax.validation.Valid;
import java.util.logging.Logger;


@Controller
public class RecipeController {

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(RecipeController.class);

    private final RecipeCommandService recipeCommandService;
    private final RecipeQueryService recipeQueryService;

    public RecipeController(RecipeCommandService recipeCommandService, RecipeQueryService recipeQueryService) {
        this.recipeCommandService = recipeCommandService;
        this.recipeQueryService = recipeQueryService;
    }

    @RequestMapping(value = "/addrecipe", method = RequestMethod.GET)
    public String recipe(Model model) {
        model.addAttribute("recipeForm", new Recipe());

        return "recipeForm";
    }

    @RequestMapping(value = "/addrecipe", method = RequestMethod.POST)
    public String addupdaterecipe(@ModelAttribute("recipeForm") Recipe recipe, Model model) {

        if (recipe.getId() == null) {
        recipeCommandService.create(recipe);

        } else {
            recipeCommandService.update(recipe);
        }
        model.addAttribute("success", "Recipe" + recipe.getRecipe_title() + " " + recipe.getRecipe_author() + " added successfully");
        //return "success";
        return "recipeFormSuccess";
    }
}