<%--<%@ taglib prefix="spring" uri="http://java.sun.com/jsp/jstl/fmt" %>--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/register.css"/>

<!DOCTYPE html>
<html lang="en">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><spring:message code="application.title"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <h2><spring:message code="application.title"/></h2>

    <div class="right">
        <c:url var="logoutUrl" value="/perform_logout"/>
        <spring:message code="logout.submit.label" var="labelSubmit"/>
        <form:form method="post" action="${logoutUrl}">
            <input class="btn btn-lg btn-success" type="submit" value="${labelSubmit}"/>
        </form:form>
    </div>
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
        <li data-target="#myCarousel" data-slide-to="4"></li>
        <li data-target="#myCarousel" data-slide-to="5"></li>
        <li data-target="#myCarousel" data-slide-to="6"></li>
        <li data-target="#myCarousel" data-slide-to="7"></li>
        <li data-target="#myCarousel" data-slide-to="8"></li>
        <li data-target="#myCarousel" data-slide-to="9"></li>
        <li data-target="#myCarousel" data-slide-to="10"></li>
        <li data-target="#myCarousel" data-slide-to="11"></li>
        <li data-target="#myCarousel" data-slide-to="12"></li>
        <li data-target="#myCarousel" data-slide-to="13"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">

        <div class="item active">
            <img src="${pageContext.request.contextPath}/resources/images/food1.jpg" alt="Los Angeles" style="width:100%;">
            <div class="carousel-caption">
                <h3>Przekąski</h3>
                <p>Sam przygotuj</p>
            </div>
        </div>

        <div class="item">
            <img src="${pageContext.request.contextPath}/resources/images/food2.jpg" alt="Chicago" style="width:100%;">
            <div class="carousel-caption">
                <h3>Grill</h3>
                <p>Thank you, Chicago!</p>
            </div>
        </div>

        <div class="item">
            <img src="${pageContext.request.contextPath}/resources/images/food3.jpg" alt="New York" style="width:100%;">
            <div class="carousel-caption">
                <h3>New York</h3>
                <p>We love the Big Apple!</p>
            </div>
        </div>
        <div class="item">
            <img src="${pageContext.request.contextPath}/resources/images/food14.jpg" alt="Los Angeles" style="width:100%;">
            <div class="carousel-caption">
                <h3>Zupy</h3>
                <p>Sam przygotuj</p>
            </div>
        </div>

        <div class="item">
            <img src="${pageContext.request.contextPath}/resources/images/food5.jpg" alt="Chicago" style="width:100%;">
            <div class="carousel-caption">
                <h3>Sałatki</h3>
                <p>Thank you, Chicago!</p>
            </div>
        </div>

        <div class="item">
            <img src="${pageContext.request.contextPath}/resources/images/food13.jpg" alt="New York" style="width:100%;">
            <div class="carousel-caption">
                <h3>New York</h3>
                <p>We love the Big Apple!</p>
            </div>
        </div>
        <div class="item">
            <img src="${pageContext.request.contextPath}/resources/images/food7.jpg" alt="Los Angeles" style="width:100%;">
            <div class="carousel-caption">
                <h3>Przekąski</h3>
                <p>Sam przygotuj</p>
            </div>
        </div>

        <div class="item">
            <img src="${pageContext.request.contextPath}/resources/images/food8.jpg" alt="Chicago" style="width:100%;">
            <div class="carousel-caption">
                <h3>Grill</h3>
                <p>Thank you, Chicago!</p>
            </div>
        </div>

        <div class="item">
            <img src="${pageContext.request.contextPath}/resources/images/food6.jpg" alt="New York" style="width:100%;">
            <div class="carousel-caption">
                <h3>New York</h3>
                <p>We love the Big Apple!</p>
            </div>
        </div> <div class="item">
            <img src="${pageContext.request.contextPath}/resources/images/food10.jpg" alt="Los Angeles" style="width:100%;">
            <div class="carousel-caption">
                <h3>Przekąski</h3>
                <p>Sam przygotuj</p>
            </div>
        </div>
        <div class="item">
            <img src="${pageContext.request.contextPath}/resources/images/food11.jpg" alt="Chicago" style="width:100%;">
            <div class="carousel-caption">
                <h3>Grill</h3>
                <p>Thank you, Chicago!</p>
            </div>
        </div>

        <div class="item">
            <img src="${pageContext.request.contextPath}/resources/images/food12.jpg" alt="New York" style="width:100%;">
            <div class="carousel-caption">
                <h3>New York</h3>
                <p>We love the Big Apple!</p>
            </div>
        </div>
        <div class="item">
            <img src="${pageContext.request.contextPath}/resources/images/food9.jpg" alt="Chicago" style="width:100%;">
            <div class="carousel-caption">
                <h3>Grill</h3>
                <p>Thank you, Chicago!</p>
            </div>
        </div>

        <div class="item">
            <img src="${pageContext.request.contextPath}/resources/images/food4.jpg" alt="New York" style="width:100%;">
            <div class="carousel-caption">
                <h3>New York</h3>
                <p>We love the Big Apple!</p>
            </div>
        </div>

    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
    <div class="addrecipe"><a href="${pageContext.request.contextPath}/addrecipe/" class="btn btn-primary btn-lg">Add recipe</a>
    </div>
    <%--stąd są przepisy--%>
    <div class="row">
        <div class="col-sm-4">
            <h3>Spaghetti</h3>
            <p>Amore pomidoro...</p>
            <img src="${pageContext.request.contextPath}/resources/images/talerz1.png"/>
            <p>Ze śliwkami</p>
        </div>
        <div class="col-sm-4">
            <h3>Frytki z Colą</h3>
            <p>z McDonald'sa</p>
            <img src="${pageContext.request.contextPath}/resources/images/talerz2.png"/>
            <p>i keczup</p>
        </div>
        <div class="col-sm-4">
            <h3>Column 3</h3>
            <p>Lorem ipsum dolor..</p>
            <img src="${pageContext.request.contextPath}/resources/images/talerz3.png"/>
            <p>Ut enim ad..</p>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <h3>Klopsiki w sosie własnym</h3>
            <p>Lorem ipsum dolor..</p>
            <img src="${pageContext.request.contextPath}/resources/images/talerz4.png"/>
            <p>Ut enim ad..</p>
        </div>
        <div class="col-sm-4">
            <h3>Column 2</h3>
            <p>Lorem ipsum dolor..</p>
            <img src="${pageContext.request.contextPath}/resources/images/talerz5.png"/>
            <p>Ut enim ad..</p>
        </div>
        <div class="col-sm-4">
            <h3>Column 3</h3>
            <p>Lorem ipsum dolor..</p>
            <img src="${pageContext.request.contextPath}/resources/images/talerz6.png"/>
            <p>Ut enim ad..</p>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <h3>Column 1</h3>
            <img src="${pageContext.request.contextPath}/resources/images/talerz7.png"/>
            <p>Lorem ipsum dolor..</p>
            <p>Ut enim ad..</p>
        </div>
        <div class="col-sm-4">
            <h3>Column 2</h3>
            <p>Lorem ipsum dolor..</p>
            <img src="${pageContext.request.contextPath}/resources/images/talerz8.png"/>
            <p>Ut enim ad..</p>
        </div>
        <div class="col-sm-4">
            <h3>Column 3</h3>
            <p>Lorem ipsum dolor..</p>
            <img src="${pageContext.request.contextPath}/resources/images/talerz1.png"/>
            <p>Ut enim ad..</p>
        </div>
    </div>
</div>
</body>
</html>