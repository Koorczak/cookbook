<%@ page isELIgnored="false" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>User Registration Form</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/register.css"/>
</head>

<body>
<div id="container">
    <div class="flags"><a href="${pageContext.request.contextPath}/register?language=pl">
        <img src="${pageContext.request.contextPath}/resources/images/PL.png" hspace="5"></a>
        <a href="${pageContext.request.contextPath}/register?language=en">
            <img src="${pageContext.request.contextPath}/resources/images/GB.png" hspace="5"></a>
    </div>
    <spring:message code="register.form.button.login" var="login"/>
    <div class="loginregister"><a href="${pageContext.request.contextPath}/login"
                                  class="btn btn-lg btn-success">${login}</a></div>
    <div class="ralative">
        <img src="${pageContext.request.contextPath}/resources/images/login_register.jpg" width="100%">
    </div>
    <div class="absolute">
        <form:form id="signup" modelAttribute="registration" action="${pageContext.request.contextPath}/register">
            <h2><spring:message code="login.title"/></h2>

            <div class="regsuccess">
                    ${success}
            </div>
            <spring:message code="register.form.firstname" var="firstname"/>
            <form:input type="text" placeholder="${firstname}" path="firstName"/>
            <form:errors path="firstName" cssClass="error"/>
            <spring:message code="register.form.lastname" var="lastname"/>
            <form:input type="text" placeholder="${lastname}" path="lastName"/>
            <form:errors path="lastName" cssClass="error"/>
            <spring:message code="register.form.email" var="email"/>
            <form:input type="email" placeholder="${email}" path="email"/>
            <form:errors path="email" cssClass="error"/>
            <spring:message code="register.form.password" var="password"/>
            <form:input type="password" placeholder="${password}" path="password"/>
            <form:errors path="password" cssClass="error"/>
            <spring:message code="register.form.confirmpassword" var="confirmpassword"/>
            <form:input type="password" placeholder="${confirmpassword}" path="confirmPassword"/>
            <form:errors path="confirmPassword" cssClass="error"/>
            <div class="error">
                <form:errors/>
            </div>
            <spring:message code="register.form.button.register" var="register"/>
            <form:button type="submit">${register}</form:button>

        </form:form>
    </div>
</div>
</body>
</html>