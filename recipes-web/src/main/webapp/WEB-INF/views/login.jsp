<%@ page isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><spring:message code="application.title"/></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/register.css"/>

</head>
<body>
<div id="container">
    <div class="flags"><a href="${pageContext.request.contextPath}/login?language=pl">
        <img src="${pageContext.request.contextPath}/resources/images/PL.png" hspace="5"></a>
        <a href="${pageContext.request.contextPath}/login?language=en">
        <img src="${pageContext.request.contextPath}/resources/images/GB.png" hspace="5"></a>
    </div>
    <div class="loginregister"><a href="${pageContext.request.contextPath}/register" class="btn btn-lg btn-success"><spring:message code="register.submit.label"/></a></div>
    <div class="relative">
        <img src="${pageContext.request.contextPath}/resources/images/login_register.jpg" width="100%">
    </div>
    <div class="absolute">
        <form:form id="signup" action="perform_login">
        <h2><spring:message code="login.title"/></h2>
        <input type="email" placeholder="<spring:message code="login.label.email"/>" required="" name="email">
        <input type="password" placeholder="<spring:message code="login.label.password"/>" required="" name="password">
        <button type="submit"><spring:message code="login.submit.label"/></button>
    </form:form>
    </div>
</div>
</body>
</html>