<%@ page isELIgnored="false" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
      integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/register.css"/>

<!DOCTYPE html>
<html>
<head>
    <title>Recipe form/Formularz przepisu</title>
    <script src="${pageContext.request.contextPath}/resources/js/form.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
          integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

</head>
<body>
<div class="container">
    <h2><spring:message code="application.title"/></h2>

    <div class="right">
        <c:url var="logoutUrl" value="/perform_logout"/>
        <spring:message code="logout.submit.label" var="labelSubmit"/>
        <form:form method="post" action="${logoutUrl}">
            <input class="btn btn-lg btn-success" type="submit" value="${labelSubmit}"/>
        </form:form>
    </div>
    <div class="row">
        <div class="col-md-4"><img src="${pageContext.request.contextPath}/resources/images/add_recipe.png"></div>
        <div class="col-md-8">
            <div class="relative_col">
            <form:form id="addform" modelAttribute="recipeForm" action="${pageContext.request.contextPath}/addrecipe">
                <h1>Recipe title: </h1><input type="text" name="recipe_title">
                <br>
                <h1>Recipe author: </h1><input type="text" name="recipe_author">
                <br>
                <h1>Recipe category: </h1>
                <input type="radio" checked name="recipe_group" value="desert/cake" >Desert/Cake
                <input type="radio" name="recipe_group" value="dinner" >Dinner
                <input type="radio" name="recipe_group" value="breakfast" >Breakfast
                <%--<input type="text" name="Recipe category">--%>
                <p>
                <table class="table" id="myTable">
                    <thead>
                    <tr>
                        <th>
                            <h2>Dodaj swój składnik</h2>
                        </th>
                    </tr>
                    </thead>
                    <body>
                    <tr id="row0">
                        <td>
                            <div class="input-group">
                                <input type="text" class="form-control" name="recipe_ingredient"/>
                                <span class="input-group-btn">
                        <button id="btn0" type="button" class="btn btn-primary" onclick="addRow(this)">
                            <span id="icon0" class="glyphicon glyphicon-plus"></span>
                        </button>
                    </span>
                            </div>
                        </td>
                    </tr>
                    </body>
                </table>
                <br>
                <form:button type="submit">Zapisz przepis!</form:button>
                <%--<button type="submit"><spring:message code="form.submit.label"/></button>--%>
            </form:form>
        </div>
    </div>
</div>
<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="//malsup.github.com/jquery.form.js"></script>
</body>
</html>