package pl.sda.recipes.service.user.query;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.recipes.dao.domain.User;
import pl.sda.recipes.dao.repository.UserRepository;

import javax.security.auth.login.AccountNotFoundException;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class UserQueryService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserQueryService.class);

    private final UserRepository userRepository;

    @Autowired
    public UserQueryService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    public List<User> findAll() {
        return (List<User>) userRepository.findAll();
    }

    public User findUserByEmail(String email){
        return userRepository.findByEmail(email);
    }


    public User findById(Long id) throws AccountNotFoundException {
        User user  = userRepository.findOne(id);
        if (user == null) {
            LOGGER.debug("Account with id " + id + " not found.");
            throw new AccountNotFoundException();
        }

        return user;
    }
}
