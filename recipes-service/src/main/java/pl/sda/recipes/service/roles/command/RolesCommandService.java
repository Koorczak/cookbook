package pl.sda.recipes.service.roles.command;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.recipes.dao.domain.Roles;
import pl.sda.recipes.dao.repository.RolesRepository;

@Service
@Transactional
public class RolesCommandService {

    private final RolesRepository rolesRepository;

    public RolesCommandService(RolesRepository rolesRepository) {
        this.rolesRepository = rolesRepository;
    }

    public Long create(Roles roles) {

        rolesRepository.save(roles);
        return roles.getId();
    }


}
