package pl.sda.recipes.service.roles.query;


import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.recipes.dao.domain.Roles;
import pl.sda.recipes.dao.repository.RolesRepository;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class RolesQueryService {

    private final RolesRepository rolesRepository;

    public RolesQueryService(RolesRepository rolesRepository) {
        this.rolesRepository = rolesRepository;
    }

    public List<Roles> findAll(){
        return (List<Roles>) rolesRepository.findAll();
    }

    public Roles findRoleById (Long id) {
        Roles roles = rolesRepository.findOne(id);
        return roles;
    }
}
