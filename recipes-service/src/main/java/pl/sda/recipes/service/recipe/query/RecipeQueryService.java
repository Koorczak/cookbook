package pl.sda.recipes.service.recipe.query;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.recipes.dao.domain.Recipe;
import pl.sda.recipes.dao.repository.RecipeRepository;
import pl.sda.recipes.service.recipe.exception.RecipeNotFoundException;


import javax.security.auth.login.AccountNotFoundException;
import javax.transaction.Transactional;
import java.util.List;
@Transactional
@Service

public class RecipeQueryService {
    private static final Logger LOGGER = LoggerFactory.getLogger(RecipeQueryService.class);

    private final RecipeRepository recipeRepository;

    @Autowired
    public RecipeQueryService(RecipeRepository recipeRepository) {
        this.recipeRepository = recipeRepository;
    }


    public List<Recipe> findAll() {
        return (List<Recipe>) recipeRepository.findAll();
    }

    public Recipe findById(Long id) throws RecipeNotFoundException {
        Recipe recipe = recipeRepository.findOne(id);
        if (recipe == null) {
            LOGGER.debug("Recipe with id " + id + " not found.");
            throw new RecipeNotFoundException();
        }

        return recipe;
    }
}
