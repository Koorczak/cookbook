package pl.sda.recipes.service.recipe.command;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.recipes.dao.domain.Recipe;
import pl.sda.recipes.dao.repository.RecipeRepository;
import pl.sda.recipes.service.recipe.exception.RecipeNotFoundException;


import javax.transaction.Transactional;

@Transactional
@Service
public class RecipeCommandService {
    private static final Logger LOGGER = LoggerFactory.getLogger(RecipeCommandService.class);

    private final RecipeRepository recipeRepository;

    @Autowired
    public RecipeCommandService(RecipeRepository recipeRepository) {
        this.recipeRepository = recipeRepository;
    }

    public Long create(Recipe recipe) {

        recipeRepository.save(recipe);

        return recipe.getId();
    }

    public void update(Recipe recipe) {
        Recipe dbRecipe = recipeRepository.findOne(recipe.getId());
        if (dbRecipe == null) {
            LOGGER.debug("Recipe with id " + recipe.getId() + " not found.");
            throw new RecipeNotFoundException();
        }
        dbRecipe.setRecipe_title(recipe.getRecipe_title());
        dbRecipe.setRecipe_group(recipe.getRecipe_group());
        dbRecipe.setRecipe_author(recipe.getRecipe_author());
        dbRecipe.setRecipe_author(recipe.getRecipe_ingredient());
    }

    public void delete(Long id) {
        Recipe recipe = recipeRepository.findOne(id);
        if (recipe == null) {
            LOGGER.debug("User with id " + recipe.getId() + " not found.");
            throw new RecipeNotFoundException();
        }
        recipeRepository.delete(recipe);
    }
}
