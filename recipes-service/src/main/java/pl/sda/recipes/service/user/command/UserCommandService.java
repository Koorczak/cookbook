package pl.sda.recipes.service.user.command;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.recipes.dao.DTO.RegistrationDTO;
import pl.sda.recipes.dao.domain.Roles;
import pl.sda.recipes.dao.domain.User;
import pl.sda.recipes.dao.repository.RolesRepository;
import pl.sda.recipes.dao.repository.UserRepository;
import pl.sda.recipes.service.user.exception.UserNotFoundException;
import pl.sda.recipes.service.user.query.UserQueryService;

import java.util.Collections;
import java.util.HashSet;

@Service
@Transactional
public class UserCommandService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserCommandService.class);

    private final UserRepository userRepository;
    private final RolesRepository rolesRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final UserQueryService userQueryService;

    @Autowired
    public UserCommandService(UserRepository userRepository, RolesRepository rolesRepository, BCryptPasswordEncoder bCryptPasswordEncoder, UserQueryService userQueryService) {
        this.userRepository = userRepository;
        this.rolesRepository = rolesRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.userQueryService = userQueryService;
    }

    public Long create(RegistrationDTO registrationDTO) {

        LOGGER.debug("checking if user email exist in db");
        User userExists = userQueryService.findUserByEmail(registrationDTO.getEmail());

        if (userExists != null) {
           throw new RuntimeException("user already exist");
        }

        User user = new User();

        user.setFirstName(registrationDTO.getFirstName());
        user.setLastName(registrationDTO.getLastName());
        user.setEmail(registrationDTO.getEmail());
        user.setPassword(bCryptPasswordEncoder.encode(registrationDTO.getPassword()));
//        user.setPassword(registrationDTO.getPassword());

        Roles roles = rolesRepository.findByRole("User");
        user.setRoles(new HashSet<>(Collections.singletonList(roles)));
        user.setEnabled(true);
        LOGGER.debug("creating user in db");
        userRepository.save(user);

        return user.getId();
    }

    public void update(User user) {
        User dbUser = userRepository.findOne(user.getId());
        if (dbUser == null) {
            LOGGER.debug("User with id " + user.getId() + " not found.");
            throw new UserNotFoundException();
        }
        dbUser.setFirstName(user.getFirstName());
        dbUser.setLastName(user.getLastName());
        dbUser.setEmail(user.getEmail());
    }

    public void delete(Long id) {
        User user = userRepository.findOne(id);
        if (user == null) {
            LOGGER.debug("User with id " + user.getId() + " not found.");
            throw new UserNotFoundException();
        }
        userRepository.delete(user);
    }

}
