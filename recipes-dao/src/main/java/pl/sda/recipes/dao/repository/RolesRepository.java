package pl.sda.recipes.dao.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.sda.recipes.dao.domain.Roles;

@Repository
public interface RolesRepository extends CrudRepository<Roles, Long>{
    Roles findByRole(String role);

}
