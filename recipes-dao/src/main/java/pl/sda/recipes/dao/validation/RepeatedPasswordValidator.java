package pl.sda.recipes.dao.validation;

import pl.sda.recipes.dao.DTO.RegistrationDTO;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class RepeatedPasswordValidator implements ConstraintValidator<RepeatedPassword, RegistrationDTO> {

    @Override
    public void initialize(RepeatedPassword constraint) {
        //Nothing to do here
    }

    @Override
    public boolean isValid(RegistrationDTO registrationDTO , ConstraintValidatorContext constraintValidatorContext) {
        return registrationDTO.getPassword().equals(registrationDTO.getConfirmPassword());
    }

}
