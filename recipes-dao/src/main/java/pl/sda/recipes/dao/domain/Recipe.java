package pl.sda.recipes.dao.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table (name = "RECIPE")
public class Recipe implements Serializable {


    private static final long serialVersionUID = -3369861597837533134L;

    @Id
    @GeneratedValue
    @Column(name = "RECIPE_ID")
    private Long id;
    @Column(name = "RECIPE_TITLE")
    private String recipe_title;

    @Column(name = "RECIPE_GROUP")
    private String recipe_group;

    @Column(name = "RECIPE_AUTHOR")
    private String recipe_author;

    @Column(name = "RECIPE_INGREDIENT")
    private String recipe_ingredient;

    public Recipe(String recipe_title, String recipe_group, String recipe_author, String recipe_ingredient) {
        this.recipe_title = recipe_title;
        this.recipe_group = recipe_group;
        this.recipe_author = recipe_author;
        this.recipe_ingredient = recipe_ingredient;
    }

    public Recipe() {

    }

    public Long getId() {
        return id;
    }

    public String getRecipe_title() {
        return recipe_title;
    }

    public String getRecipe_group() {
        return recipe_group;
    }

    public String getRecipe_author() {
        return recipe_author;
    }

    public String getRecipe_ingredient() {
        return recipe_ingredient;
    }

    public void setRecipe_title(String recipe_title) {
        this.recipe_title = recipe_title;
    }

    public void setRecipe_group(String recipe_group) {
        this.recipe_group = recipe_group;
    }

    public void setRecipe_author(String recipe_author) {
        this.recipe_author = recipe_author;
    }

    public void setRecipe_ingredient(String recipe_ingredient) {
        this.recipe_ingredient = recipe_ingredient;
    }
}

